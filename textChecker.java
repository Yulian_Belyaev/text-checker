import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 23.06.2017.
 */
public class textChecker {
    public static void main(String[] args) throws IOException {
        File result = new File("src\\Result.txt");
        String string;
        Pattern pattern = Pattern.compile("\\b[A-z]\\w*\\b");
        Matcher matcher = pattern.matcher("**");
        Pattern commentPattern = Pattern.compile("[*][ ][A-zА-я0-9]*");
        Matcher comment = commentPattern.matcher("**");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(result)); BufferedReader bufferedReader = new BufferedReader(new FileReader(result))){
            while ((string = bufferedReader.readLine()) != null){
                matcher.reset(string);
                comment.reset(string);
                while (!comment.find() & matcher.find()) {
                    bufferedWriter.write(matcher.group() + "\n");
                }
            }
        }
    }
}
